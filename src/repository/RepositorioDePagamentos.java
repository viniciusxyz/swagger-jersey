package repository;

import java.util.ArrayList;
import java.util.UUID;

import com.sun.jersey.spi.resource.Singleton;

import model.Pagamento;

@Singleton
public class RepositorioDePagamentos {

	private ArrayList<Pagamento> repositorio = new ArrayList<>();
	
	public RepositorioDePagamentos() {

	}

	public Pagamento busca(Integer id) {
		return repositorio.get(id);
	}

	public void cria(Pagamento pagamento) {
		UUID idPagamento = java.util.UUID.randomUUID();
		pagamento.setId(idPagamento);
		repositorio.add(pagamento);
	}
	
	public String coletarTodosPagamentos()
	{
		return repositorio.toString();
	}
}

