package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.spi.inject.Inject;

import model.Pagamento;
import repository.RepositorioDePagamentos;

@Path("/pagamentos")
public class PagamentoResource {

	@SuppressWarnings("deprecation")
	@Inject private RepositorioDePagamentos repositorioDePagamentos;
	
	@GET
	public String demostrarTodosPagamentos()
	{
		return this.repositorioDePagamentos.coletarTodosPagamentos();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registraPagamento(Pagamento pagamento)
	{
		System.out.println(pagamento);
		this.repositorioDePagamentos.cria(pagamento);
		return Response.ok("{\"mensagem\":\"Pagamento efetuado\"}", MediaType.APPLICATION_JSON).build();
	}
}
