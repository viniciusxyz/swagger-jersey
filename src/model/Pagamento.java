package model;

import java.math.BigDecimal;
import java.util.UUID;

import com.google.gson.Gson;

public class Pagamento {
	
  private static final String STATUS_CRIADO = "CRIADO";
  private static final String STATUS_CONFIRMADO = "CONFIRMADO"; 
  private static final String STATUS_CANCELADO = "CANCELADO";
	
  private UUID id;
  private String status;
  private BigDecimal valor;
  
  	
	
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
		// return "status=" + status + ", valor=" + valor + "]";
	}
	 
	  
	}
